#!/bin/bash

# Before running this script, take screen captures with chromium and place in this folder.

# Do not try to parse if there are no matching files.
shopt -s nullglob

# Enter the cards directory.
cd cards/

# For each image in the current folder..
for type in *.png
do
	# Log which image we are working on.
	echo "Processing ${type}.."

	# Create folder to store result, if necessary.
	mkdir -p ./${type%.*}/

	# Split screencapture into cards:
	convert ${type} -crop 825x1125 -repage 0x0 -scene 1 ./${type%.*}/card-%02d.png
done

# Move back to current directory.
cd ..
