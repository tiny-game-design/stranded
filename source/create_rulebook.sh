#!/bin/bash

# Log that we are creating a rule book.
echo "Creating 8-page rule booklet.."

# Enter the cards directory.
cd cards/

# Create a temporary directory to hold split pages.
mkdir -p split

# Convert the rulebook images to individual PDF page files.
convert rules.png -crop 825x1125 -repage 0x0 -scene 1 split/page-%02d.pdf

# Remove the excess pages as it won't be part of the rulebook.
#rm split/page-01.pdf

# Convert the individual rule pages into a single PDF file.
convert split/* ../rulebook.pdf

# Remove temporary folder.
rm -rf split/

# Move back to current directory.
cd ..
