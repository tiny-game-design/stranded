# Stranded!

Silly in nature and basic in gameplay, Stranded! still offers some depth and hopefully a giggle or two over the somewhat absurd situations you will find yourself in.

The game is easy to learn with a simple turn order and clear rules. It is best played 3-4 people but can be played with only 2, or in a larger group up to 5 players.

Your goal is to survive, with other players, while being stranded on a mysterious island with limited food. Using your intellectual and physical prowess, you can choose to either play cooperatively with your fellow friends, or competetively and do everything you can to make 'em starve to death!

Can you survive all four seasons and get off the island alive?
Get Stranded! today, and find out. (yes, pun intended)

You can buy hard copies of <b>Stranded!</b> on <a href='https://www.thegamecrafter.com/games/stranded-1'>thegamecrafter.com</a>
